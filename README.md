# xlsx-checker 使用文档

## 简介

xlsx-checker 是一个用于在前端页面校验 xlsx 表格内容的工具，工具依赖 xlsx.js。

注意：

> 包名中不带有 min 的版本，打包时已将 xlsx.js 打入其中，方便直接引入使用
>
> 包名中带有 min 的版本在打包时，已排除 xlsx.js，故使用时需单独安装 xlsx.js
>
> xlsx-checker.js 导出一个函数方法用于校验
>
> xlsx-checker-worker.js 通过使用 web-worker 的方式进行校验，整合了简单的 worker 调用，通过 postMessage 返回校验结果。

## 安装

NPM

```shell
npm i xlsx-checker
```

### Browser

```html
<script lang="javascript" src="dist/xlsx-checker.js"></script>
```

或

```html
<script lang="javascript" src="dist/xlsx-checker-worker.js"></script>
```

## 使用

配置项

```javascript
let options = {
  offset: {
    // 偏移，不校验第一行第一列
    row: 1,
    col: "A",
  },
  rules: {
    Sheet1: {
      // 表格名称
      A: "number", // A列数字
      B: "integer", // B列整数
      C: "decimal", // C列小数
      D: "fraction", // D列分数
      E: "calc((A+B)/C - D)", // E列为ABCD列的计算值
      F: "includes([12,30])", // F列为其中之一
    },
    Sheet2: {}, // 第二张表
  },
  outputFile: true,
};
```

校验

```javascript
xlsxChecker(file, options).then((res) => {
  console.log("res1:", res);
  // xlsxChecker.saveBolbToFileInWeb(res.blob, "output.xlsx"); 配置中未开启输出文件，或输出失败客尝试使用此方法重新输出
});
```

使用 worker

```javascript
var myWorker = new Worker("../dist/xlsx-checker-worker.js");
myWorker.onmessage = (event) => {
  console.log(event.data);
};

myWorker.onerror = (error) => {
  myWorker.terminate();
  console.log(error.filename, error.lineno, error.message); // 发生错误的文件名、行号、错误内容
};
myWorker.postMessage({ file, options });
```

## 配置项说明

| 参数       | 类型    | 必需 | 说明                      |
| ---------- | ------- | ---- | ------------------------- |
| offset     | object  | 否   | 偏移量，设置起始行列      |
| rules      | object  | 是   | 校验规则，按 表格-列 设置 |
| outputFile | Boolean | 否   | 是否直接下载校验结果      |

### rules `<object>`

以 sheet 名称为键的对象，切该对象以列名（ABCD 等）为键，以校验方法为值

示例：

```javascript
{
    Sheet1: {
      // 表格名称
      A: "number", // A列数字
      B: "integer", // B列整数
      C: "decimal", // C列小数
      D: "fraction", // D列分数
      E: "calc((A+B)/C - D)", // E列为ABCD列的计算值
      F: "includes([12,30])", // F列为其中之一
    },
}
```

### offest `<object>`

| 参数 | 类型          | 必需 | 说明                               |
| ---- | ------------- | ---- | ---------------------------------- |
| row  | string/number | 否   | 行偏移，值为行号，表示从第几行开始 |
| col  | string        | 否   | 列偏移，值为列名，表示从第几列开始 |

## 内置校验方法

### 简单校验

简单校验不需要传参或获取其他值，仅校验当前单元格的值或格式

#### number

是否是数字

#### integer

是否是整数

#### decimal

是否是小数

#### fraction

是否是分数

#### boolean

是否是布尔值或字符串的 true 或 false

#### science

是否是科学计数法的值

#### rightDate

是否是正确的日期，如平年 2 月 29 日为错误日期

### 传参校验

传参校验, 需要传入参数的校验。

不过为了获取参数方便且于简单校验保持一定的统一，传参校验仍然以字符串形式传入

如： `A: "dateIsFormat('yyyy-MM-dd')"`

#### dateIsFormat(format)

校验单元格日期是否符合 format 格式,

完整格式示例：yyyy-MM-dd hh:mm:

#### regexp(pattern)

传入正则进行校验

##### lt(target)

判断单元格的值是否小于 target

less than target

#### lte(target)

判断单元格的值是否小于等于 target

#### gt(target)

判断单元格的值是否大于 target

#### gte(target)

判断单元格的值是否大于等于 target

#### eq(target)

判断单元格的值是否等于 target

#### ne(target)

判断单元格的值是否不等于 target

#### startsWith(str)

判断单元格的值是否以 str 开头

#### endsWith(str)

判断单元格的值是否以 str 结尾

#### includes([a,b,c])

判断单元格的值是否包含 a,b,c 之一

例如：当 cell="string"时，`include(['stc','ing','ddd']) `返回 `true`。 因为 string 包含 ing。

如果需要判断 cell 的值是否为给定中的一个，请使用 or

#### between([min,max])

判断单元格的值是否在 min max 之间，包含 min max

#### notBetween

判断单元格的值是否不在 min max 之间，不包含 min max

#### or([a,b,c])

判断单元格的值是否是啊，a,b,c 其中之一

#### calc((A+B)/C - D)

判断单元格的值是否是同行其他列数据的计算值，其中列名必须大写

## 自定义校验方法

rules 支持自定义校验方法，只需要在对应的列名以对象的形式传入即可，示例：

```javascript
{
    Sheet1: {
      A: {
	verify: function(cell, colDatas){
          if (校验通过) {
            return true;
          } else {
            return false;
          }
	},
	cols: ["A","B"],
      }
    },
}
```

### 字段说明

verify: 自定义校验函数

    参数：

    cell：当前列某单元格数据，

    colDatas`<object>`：cell单元格同行的数据，key为列名，由cols数组决定

cols: 校验所需的同行的其他列，传入列名数组。传入字符串 "all" 表示所有列（能读取到的数据列，不包含 offset）
